"""
關於第十六章投影片用的範例
"""


def ex16_1():
    import re
    phoneRule = re.compile(r'\d{4}-\d{3}-\d{3}')  
    print("regex test")
    #seach只有一個不然就是None
    result=phoneRule.search("0915-564-987")
    print(result.group())
    result=phoneRule.search("1")
    print(result)
    result=phoneRule.findall("0915-564-98741215-564-9987")
    print(result)
    print("{:*^20} ".format(""))
    result=re.findall(r'\d{4}-\d{3}-\d{3}',"0915-564-98741215-564-9987")
    print(result)
    result=re.findall(r'(\d{4})-(\d{3})-(\d{3})',"0915-564-98741215-564-9987")
    print(result)    
    print("{:*^20} ".format(""))
    # ()表示分群
    result=re.search(r'(\d{4})-(\d{3})-(\d{3})',"0915-564-98741215-564-9987")
    print(result.group())
    print(result.group(0))
    print(result.group(1))
    print(result.group(2))
    print(result.group(3))
    print("{:*^20} ".format(""))
    #多一個s
    a,b,c=result.groups()
    print(a)
    print(b)
    print(c)

def ex16_2():
    import re
    result=re.search(r'(\((\d{2})\))(\d{4})-(\d{4})',"(02)2896-4896")
    print("regex test")
    print(result)
    if result != None :  print(result.group())
       
    print("{:*^20} ".format(""))
    pattern = r'(Mary|Tom) is at home'
    result=re.search(pattern,"Mary is at home")
    if result != None :  print(result.group())
    result=re.search(pattern,"Tom is at home")
    if result != None :  print(result.group())

    print("{:*^20} ".format(""))
    pattern = "(John)(son|nason|nathan)"
    #沒加()的話會不顯示
    #pattern = "John(son|nason|nathan)"
    result=re.findall(pattern,"Johnson, Johnnason and Johnnathan will attend my party tonight.")
    print(result)

def ex16_3():
    import re
    result=re.search(r'(John)(na)?son',"Johnson will attend my party tonight.")
    if result != None :  print(result.group())
    result=re.search(r'(John)(na)*son',"Johnnanananason will attend my party tonight.")
    if result != None :  print(result.group())
    result=re.search(r'(John)(na)+son',"Johnnanananason will attend my party tonight.")
    if result != None :  print(result.group())

    print("{:*^20} ".format(""))
    result=re.search(r'(John)(na)?son',"JOHNSON will attend my party tonight.",re.I)
    if result != None :  print(result.group())

def ex16_4():
    import re
    result=re.search(r'(son){3,5}',"sonsonsonsonson")
    if result != None :  print(result.group())
    result=re.search(r'(son){3,5}?',"sonsonsonsonson")
    if result != None :  print(result.group())

def ex16_5():
    import re
    result=re.search(r'\d+\s{2}\w+',"456789  asdas_ihfioe_456")
    if result != None :  print(result.group())





def ex16_6():
    import re
    result=re.search(r'^[a-zA-Z]{8}$',"aJUhyioK")
    if result != None :  print(result.group())
    result=re.search(r'Test...',"TestJIO88")
    if result != None :  print(result.group())
    result=re.search(r'Test.*123',"TestJIO88123")
    if result != None :  print(result.group())

def ex16_7():
    import re
    result=re.search(r'Test.*',"TestJIO88123 \n jegojkepgjepogjeporgjrep")
    if result != None :  print(result.group())
    result=re.search(r'Test.*',"TestJIO88123 \n jegojkepgjepogjeporgjrep",re.DOTALL)
    if result != None :  print(result.group())

def ex16_8():
    import re
    #match一定要一開始就符合
    result=re.match(r'Test',"Test jfiejof")
    if result != None :  print("match",result),print(result.start(),result.end(),result.span())
    result=re.search(r'Test',"1234 Test jfiejof")
    if result != None :  print("search:",result),print(result.start(),result.end(),result.span())
    print("{:*^20} ".format(""))
    result=re.sub(r'Test'," WTF ","1234 Test jfiejof")
    print(result)

def ex16_9():
    import re
    pattern=r'''(
        [a-z0-9_.]+     #帳號
        @               #小老鼠
        [a-z0-9_.]+     #主機
        [\.]            #.
        [a-z]{2,4}      # COM EDU
        ([\.])?         #可能有點    
        ([a-z]{2,4})?   #TW
        )'''

    import re
    result=re.search(pattern,"123AAA@yahoo.com.tw",re.DOTALL|re.I|re.VERBOSE)
    if result != None :  print(result.group())


def ex16_10():
    import re
    print("start pattern match")
   
    result=re.search(r'(?:abc)(1234)',"abc1234c")
    #1234會記錄 abc 則不會
    if result != None :  print("?:",result.groups())

    result=re.search(r'(?i)(ABC)',"1234abc1234c")
    if result != None :  print("?imx:",result.group())

    result=re.search(r'((a|b)123)',"a123")
    if result != None :  print("?(id/name):",result.group())

    print("{:*^20} ".format(""))
    #距離要為0
    m = re.search('b(?=.*c)', 'ab 1 c')
    if m != None :  print("?=:",m.group())
    m = re.search('b(?!.*c)', 'cb 1 d')    
    if m != None :  print("?!:",m.group())

    print("{:*^20} ".format(""))

    m = re.search('(?<=a).*b','a   bc')
    if m != None :  print("?<=:",m.group())    
    m = re.search('(?<!a).*b','a ba')
    if m != None :  print("?<!:",m.group())

    print("{:*^20} ".format(""))    
    m = re.search('(?P<test>a|b|c)123(?P=test)', 'abc1234c')
    if m != None :  print("?=:",m.group())


def ex16_11():
      input_data='''
                 Telnet03=Telnet Login		||Telnet-Login		||BDFM25	||User IP=192.168.1.1,Port=23,Start KW=,Login User=,Pass KW=,Login Password=a,Console KW =root@OpenWrt,Local IP=,Enter=T,Type=NET
	             Telnet04=QCMBR Start		||TelnetScript01	||INTE08	||;   
	             Delay005=Wait 3 sec for QCMBR	||DelayEvent		||		||Option=3000;
	             Telnet06=List Task		||TelnetScript02	||INTE08	||;   
               '''
      pattern=r'''(
        ^(.*)=                            #item=     
        (.*?)\|{2}(.*?)\|{2}(.*?)\|{2}    #content
        (.*?)$                            #argv        
        )'''
      import re
      list_2d=[]
      for line in input_data.splitlines():
          result=re.search(pattern,line,re.X|re.M)
          if result != None :
            list_a=[result.group(x).strip() for x in range(2,6)]            
            temp_string=re.sub(';','',result.group(6)).strip()
            if(len(temp_string)!=0):
                dict_a=dict(x.split('=', 1)  for  x in result.group(6).split(','))
            else:
                dict_a=dict()                         
            list_a.append(dict_a)
            list_2d+=[list_a]   
      print("test2d:",list_2d)
      print('*'*50)
      



      
      
def ex16_12():
    input_data="123,456,789,222,333"      
    import re
    result=re.search(r'(.*?),(.*?),(.*?),(.*?),',input_data,re.I|re.X|re.M)
    if result != None  :
        print( result.groups())
    print(input_data.split(','))
          

if __name__=='__main__':
    ex16_11()