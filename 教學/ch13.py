"""
關於第十三章投影片用的範例
"""
#不能在func裡面用
#from Temp.test import *

def ex13_1():
    from Temp import test
    
    test.make_icecream("milk","candy")
    test.make_drink("large","greentea")
 
    from Temp.test import make_icecream
    make_icecream("bean","pea")
 
    from Temp.test import add,sub
    print(add(3,5))
    print(sub(3,5))
    
    #print(mul(3,5))
    #print(div(3,5))

def ex13_2():
     from Temp.test import add as 加法器
     print(加法器(3,3))
     from Temp import test as t
     print(t.sub(7,5))
    
#from Temp.test import *
def ex13_3():
   from Temp.test import Bank,Bank2
   import Temp.test
   class_1=Bank("123",456)
   class_2=Bank2()
   class_3=Temp.test.Bank3()
   print(class_3.name)


def ex13_4():
    from Temp.test2 import Speicalbank
    class_a=Speicalbank("Lin",789)
    print(class_a.name)
    

def ex13_5():
    import random
    for i in range(3):
        print(random.randint(1,100))
        print(random.randint(500,1000))
        print(random.randint(2000,3000))

    list_a=list(range(10))
    for i in list_a:
         print(i,random.choice(list_a))
    random.shuffle(list_a)
    for i in list_a:
        print("shuffle",i)
    for i in random.sample(list_a,5):
        print("sample:",i)
    for i in range(3):
        print(random.uniform(1,100))
        print(random.uniform(500,1000))
        print(random.uniform(2000,3000))
    for i in range(3):
        print(random.random())

       

    

def ex13_6():
    import time
    temp=time.time()
    print("time:",time.time())
    time.sleep(0.3)
    print("time cost:",time.time()-temp)
    print("ASC TIME:",time.asctime( ))
    print("local TIME:",time.localtime( ))

def ex13_7():
    import sys
    print(sys.version)
    print(sys.version_info)
    
    print("please enter somthing:")
    
    msg=sys.stdin.readline()
    print(msg)
    print("please enter somthing lenth above 4:")
    msg=sys.stdin.readline(4)
    print(msg)
    sys.stdout.write("STDOUT TEST\n")

    print(sys.platform)
    for path in dir(sys.path):
        print(path)


    print("WINDOWS VERSION:",sys.getwindowsversion())

    print("executable path:",sys.executable)
    print("set recurs:",sys.setrecursionlimit(100))
    print("get recurs:",sys.getrecursionlimit())
    print("argv:",sys.argv)
    
def ex13_8():
    import keyword
    print(keyword.kwlist)
    list_a=['as','while','test','phthon']
    for x in list_a:
        print(x,keyword.iskeyword(x))

def ex13_9():
    import calendar
    print(calendar.isleap(2020))
    print(calendar.isleap(2019))
    print(calendar.month(2020,1))
    print(calendar.calendar(2020))

def val():
    return 1000

def ex13_10():
    from collections import defaultdict
    list_a= defaultdict(int)
    list_a["apple"]=20
    list_a["orange"]
    print(list_a)
    list_b= defaultdict( val  )
    list_b["apple"]=20
    list_b["orange"]
    print(list_b)

    list_c= defaultdict( lambda : 9999 )
    list_c["apple"]=20
    list_c["orange"]

    fruits=defaultdict(int)
    list_d=["apple","orange","apple"]
    for fruit in ["apple","orange","apple"]:
        fruits[fruit]+=1
    print(fruits)

    list_e=["grape","orange","orange","grape"]
    from collections import Counter
    fruit_dict=Counter(list_d)
    fruit_dict2=Counter(list_e)
    print(fruit_dict)
    print(fruit_dict.most_common())
    print(fruit_dict.most_common(0))
    print(fruit_dict.most_common(1))
    print(fruit_dict.most_common(2))

    print(fruit_dict+fruit_dict2)
    print(fruit_dict-fruit_dict2)
    print("and:",fruit_dict&fruit_dict2)
    print("or:",fruit_dict or fruit_dict2)


def ex13_11():
     from collections import deque
     deque_a=deque(range(10))
     print(deque_a)
     print(deque_a.pop())
     print(deque_a.popleft())
     deque_a.append(1000)
     deque_a.appendleft(1001)
     print(deque_a)

def ex13_12():
    import sys
    from pprint import pprint

    print("print:\n",sys.path)
    print("pprint:\n")
    pprint(sys.path)

def ex13_13():
    import itertools
    for i in itertools.chain([1,2,3],('a','d')):
        print(i)
    for i in [1,2,3]+list(('a','d')):
        print(i)
    
    for i in itertools.accumulate(range(1,6),lambda x,y:x*y):
        print(i)

    print("start cycle")
    from time import sleep
    for i in itertools.cycle([1,2,3]):
          print(i),sleep(0.3)

if __name__=='__main__':
    ex13_13()